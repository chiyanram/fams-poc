package com.fams.famspoc.domain;

import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class LoginResponse {
    private String userName;
    private String location;
    private boolean newPasswordRequired;
    private String sessionToken;
}
