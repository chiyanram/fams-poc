/**
 * \file
 * <p>
 * Oct 17, 2018
 * <p>
 * Copyright Ian Kaplan 2018
 *
 * @author Ian Kaplan, www.bearcave.com, iank@bearcave.com
 */
package com.fams.famspoc.domain;

import lombok.Builder;
import lombok.Value;

@Builder
@Value
public class UserSession {
    private final String session;
    private final String accessToken;
    private final String challengeResult;
}
