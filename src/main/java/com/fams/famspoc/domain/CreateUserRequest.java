package com.fams.famspoc.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
@JsonDeserialize(builder = CreateUserRequest.CreateUserRequestBuilder.class)
public class CreateUserRequest {
    private String userName;
    private String location;
    private String firstName;
    private String lastName;
    private String companyName;
    private String companyUrl;
    private String password;
    private Boolean isAdmin;

    @JsonPOJOBuilder(withPrefix = "")
    static class CreateUserRequestBuilder {

    }
}
