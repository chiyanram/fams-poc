package com.fams.famspoc.domain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
@JsonDeserialize(builder = Authentication.AuthenticationBuilder.class)
public class Authentication {
    private String username;
    private String password;

    @JsonPOJOBuilder(withPrefix = "")
    static class AuthenticationBuilder {

    }
}
