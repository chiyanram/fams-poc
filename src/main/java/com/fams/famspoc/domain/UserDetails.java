package com.fams.famspoc.domain;

import lombok.Builder;
import lombok.Value;

@Builder(toBuilder = true)
@Value
public class UserDetails {
    private String userName;
    private String location;
}
