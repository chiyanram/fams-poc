package com.fams.famspoc.domain;

import lombok.Builder;
import lombok.Value;

import java.util.Date;
import java.util.Map;

@Builder(toBuilder = true)
@Value
public class UserDetailInfo {
    private String userName;
    private String status;
    private Map<String, String> userAttributes;
    private Date createdDate;
    private String firstName;
    private String lastName;
    private String companyName;
    private String companyUrl;
    private String location;
    private String sessionToken;
    private Date updatedDate;
}
