package com.fams.famspoc;

public final class FamsConstants {
    public static final String LOCATION = "custom:location";
    public static final String NAME = "name";
    public static final String EMAIL = "email";


    public static final String USERNAME = "USERNAME";
    public static final String PASSWORD = "PASSWORD";
    public static final String NEW_PASSWORD = "NEW_PASSWORD";
    public static final String TEMP_PASSWORD = "3iN#bGX8";
    public static final String PASS_WORD = "PASSWORD";

    private FamsConstants() {
    }
}
