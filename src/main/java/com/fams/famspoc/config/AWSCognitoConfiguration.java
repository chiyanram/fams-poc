package com.fams.famspoc.config;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.fams.famspoc.util.Validations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AWSCognitoConfiguration {

    private final String cognitoId;
    private final String cognitoKey;

    public AWSCognitoConfiguration(
        @Value("${aws.cognitoid}") final String cognitoId,
        @Value("${aws.cognitokey}") final String cognitoKey) {

        this.cognitoId = Validations.notBlank(cognitoId, "cognitoId");
        this.cognitoKey = Validations.notBlank(cognitoKey, "cognitoKey");
    }

    @Bean
    public AWSCognitoIdentityProvider awsCognitoIdentityProvider() {
        final AWSCredentials credentials = new BasicAWSCredentials(cognitoId, cognitoKey);
        final AWSCredentialsProvider credProvider = new AWSStaticCredentialsProvider(credentials);
        return AWSCognitoIdentityProviderClientBuilder.standard()
            .withCredentials(credProvider).withRegion(Regions.US_EAST_2).build();
    }

}
