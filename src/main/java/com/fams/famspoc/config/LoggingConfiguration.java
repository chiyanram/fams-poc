package com.fams.famspoc.config;

import com.fams.famspoc.infrastructure.LoggingFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@SuppressWarnings("SpringJavaAutowiringInspection")
@Configuration
public class LoggingConfiguration {

    // We have to start our filters after the sleuth in order to have spans available
    private static final int HIGHEST_PRECEDENCE = 0;

    @Bean("loggingFilter")
    public LoggingFilter loggingFilter() {
        return new LoggingFilter();
    }

    @Bean
    public FilterRegistrationBean<LoggingFilter> loggingFilterRegistration() {

        final FilterRegistrationBean<LoggingFilter> registration = new FilterRegistrationBean<>();
        registration.setFilter(loggingFilter());
        registration.setOrder(HIGHEST_PRECEDENCE);

        return registration;
    }
}
