package com.fams.famspoc.infrastructure;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimaps;
import org.springframework.http.HttpHeaders;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import org.springframework.web.util.WebUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Logs the request and response content to the log file.
 *
 * @see org.springframework.web.filter.AbstractRequestLoggingFilter
 * @see org.springframework.web.filter.ShallowEtagHeaderFilter
 */
public class LoggingFilter extends OncePerRequestFilter {

    private final ImmutableList<String> maskedHeaders;

    private static final String DEFAULT_BEFORE_MESSAGE_PREFIX = "Received request: ";
    private static final String DEFAULT_AFTER_MESSAGE_PREFIX = "Completed request: ";
    private static final int DEFAULT_MAX_PAYLOAD_LENGTH = 10240; // 10KB
    private static final List<String> MASKED_HEADERS = Lists.newArrayList("****");

    private static final int maxPayloadLength = DEFAULT_MAX_PAYLOAD_LENGTH;

    public LoggingFilter() {
        this(ImmutableList.of(HttpHeaders.AUTHORIZATION));
    }

    public LoggingFilter(final Iterable<String> maskedHeaders) {
        this.maskedHeaders = ImmutableList.copyOf(maskedHeaders);
    }

    /**
     * The default value is "false" so that the filter may log a "before" message at the start of request processing and
     * an "after" message at the end from when the last asynchronously dispatched thread is exiting.
     */
    @Override
    protected boolean shouldNotFilterAsyncDispatch() {
        return false;
    }

    /**
     * Forwards the request to the next filter in the chain and logs the request / response. This is largely based on
     * Spring's AbstractRequestLoggingFilter.
     *
     * @see org.springframework.web.filter.AbstractRequestLoggingFilter
     */
    @Override
    protected void doFilterInternal(
        final HttpServletRequest request,
        final HttpServletResponse response,
        final FilterChain filterChain)
        throws ServletException, IOException {

        final boolean isFirstRequest = !isAsyncDispatch(request);
        final HttpServletRequest requestToUse = wrapRequestIfNecessary(request, isFirstRequest);
        final HttpServletResponse responseToUse = wrapResponseIfNecessary(response, isFirstRequest);

        final boolean shouldLog = shouldLog(requestToUse);
        if (shouldLog && isFirstRequest) {
            beforeRequest(requestToUse);
        }
        try {
            filterChain.doFilter(requestToUse, responseToUse);
        } finally {
            if (shouldLog && !isAsyncStarted(requestToUse)) {
                afterRequest(requestToUse, responseToUse);
            }
        }
    }

    /**
     * Wraps the response in ContentCachingResponseWrapper if the payload will be logged.
     *
     * @param request        the request
     * @param isFirstRequest true if this request is not currently executing within an asynchronous dispatch
     * @return the wrapped response or the original response if it did not need to be wrapped
     */
    private HttpServletResponse wrapResponseIfNecessary(
        final HttpServletResponse response,
        final boolean isFirstRequest) {

        HttpServletResponse responseToUse = response;
        if (shouldCacheResponsePayload(response, isFirstRequest)) {
            responseToUse = new ContentCachingResponseWrapper(response);
        }
        return responseToUse;
    }

    /**
     * Wraps the request in ContentCachingRequestWrapper if the payload will be logged.
     *
     * @param request        the request
     * @param isFirstRequest true if this request is not currently executing within an asynchronous dispatch
     * @return the wrapped request or the original request if it did not need to be wrapped
     */
    private HttpServletRequest wrapRequestIfNecessary(final HttpServletRequest request, final boolean isFirstRequest) {
        HttpServletRequest requestToUse = request;

        if (shouldCacheRequestPayload(request, isFirstRequest)) {
            requestToUse = new ContentCachingRequestWrapper(request);
        }
        return requestToUse;
    }

    /**
     * Returns true if the payload of the request should be captured.
     *
     * @param request        the request
     * @param isFirstRequest true if this request is not currently executing within an asynchronous dispatch
     * @return true if the payload should be captured
     */
    private boolean shouldCacheRequestPayload(final HttpServletRequest request, final boolean isFirstRequest) {
        return isFirstRequest
            && !(request instanceof ContentCachingRequestWrapper)
            && shouldCapturePayload();
    }

    /**
     * Returns true if the payload of the response should be captured.
     *
     * @param response       the response
     * @param isFirstRequest true if this request is not currently executing within an asynchronous dispatch
     * @return true if the payload should be captured
     */
    private boolean shouldCacheResponsePayload(final HttpServletResponse response, final boolean isFirstRequest) {
        return isFirstRequest
            && !(response instanceof ContentCachingResponseWrapper)
            && shouldCapturePayload();
    }

    /**
     * Returns true if the request should be logged.
     *
     * @param request the request
     * @return true if we should log this request
     */
    private static boolean shouldLog(final HttpServletRequest request) {
        return true;
    }

    /**
     * Called by doFilterInternal before the request is executed. Logs general
     *
     * @param request the request
     */
    private void beforeRequest(final HttpServletRequest request) {
        logger.info(createMessage(request, Optional.empty(), DEFAULT_BEFORE_MESSAGE_PREFIX));
    }

    /**
     * Called by doFilterInternal after the request is executed.
     *
     * @param request the request
     */
    private void afterRequest(final HttpServletRequest request, final HttpServletResponse responseToUse) {
        logger.info(createMessage(request, Optional.of(responseToUse), DEFAULT_AFTER_MESSAGE_PREFIX));
    }

    /**
     * Creates the log message.
     *
     * @param request  the request
     * @param response the response
     * @param prefix   the message prefix
     * @return a log message of the available information
     */
    private String createMessage(
        final HttpServletRequest request,
        final Optional<HttpServletResponse> response,
        final String prefix) {

        final StringBuilder message = new StringBuilder()
            .append(prefix)
            .append("[")
            .append("uri={")
            .append(request.getRequestURI());

        final String queryString = request.getQueryString();
        if (queryString != null) {
            message.append('?')
                .append(queryString);
        }

        setHeaders(message, request);

        if (shouldCapturePayload()) {

            Optional.ofNullable(WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class))
                .ifPresent(requestWrapper -> {
                    final byte[] buf = requestWrapper.getContentAsByteArray();

                    if (buf.length > 0) {
                        final int length = Math.min(buf.length, maxPayloadLength);

                        String payload;
                        try {
                            payload = new String(buf, 0, length, requestWrapper.getCharacterEncoding());
                        } catch (final UnsupportedEncodingException ex) {
                            logger.warn("Unable to log request body.", ex);
                            payload = "[unknown]";
                        }
                        final String maskedPayload = payload;
                        if (logger.isDebugEnabled()) {
                            message.append(System.lineSeparator())
                                .append("request body=")
                                .append(maskedPayload);
                        }
                    }
                });

            response.map(value -> WebUtils.getNativeResponse(response.get(), ContentCachingResponseWrapper.class))
                .ifPresent(wrappedResponse -> {
                    final byte[] buf = wrappedResponse.getContentAsByteArray();

                    if (buf.length > 0) {
                        final int length = Math.min(buf.length, maxPayloadLength);
                        String payload = "";
                        try {
                            wrappedResponse.copyBodyToResponse();
                            payload = new String(buf, 0, length, wrappedResponse.getCharacterEncoding());
                        } catch (final UnsupportedEncodingException ex) {
                            logger.warn("Unable to log response body.", ex);
                            payload = "[unknown]";
                        } catch (final IOException e) {
                            throw new RuntimeException("Unable to write response.", e);
                        }
                        final String maskedPayload = payload;

                        if (logger.isDebugEnabled()) {
                            message.append(System.lineSeparator())
                                .append("response body=")
                                .append(maskedPayload);
                        }
                    }
                });
        }

        message.append("]");

        return message.toString();
    }

    private void setHeaders(final StringBuilder message, final HttpServletRequest request) {

        final HttpHeaders headers = new ServletServerHttpRequest(request).getHeaders();
        final String headerString = headers
            .entrySet()
            .stream()
            .collect(Multimaps.toMultimap(
                Map.Entry::getKey,
                entry -> maskedHeaders.contains(entry.getKey()) ? MASKED_HEADERS : entry.getValue(),
                ArrayListMultimap::create)).toString();

        message.append("}")
            .append(" headers=")
            .append(headerString);
    }

    @Deprecated
    public void setAuditingEnabled(final boolean auditingEnabled) {
        // Noop implementation
    }

    private boolean shouldCapturePayload() {
        return logger.isDebugEnabled();
    }
}