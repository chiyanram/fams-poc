package com.fams.famspoc.controller;

import com.fams.famspoc.domain.Authentication;
import com.fams.famspoc.domain.CreateUserRequest;
import com.fams.famspoc.domain.LoginResponse;
import com.fams.famspoc.domain.UserDetailInfo;
import com.fams.famspoc.services.UserService;
import com.fams.famspoc.util.Validations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/fams/v1")
@CrossOrigin(origins = "*")
public class FamsController {

    private static final Logger logger = LoggerFactory.getLogger(FamsController.class);

    private final UserService userService;

    public FamsController(final UserService userService) {
        this.userService = Validations.notNull(userService, "userService");
    }


    @PostMapping(value = "/signup", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<UserDetailInfo> createUser(@RequestBody final CreateUserRequest createUserRequest) {
        logger.info(" create user api is invoked");

        final UserDetailInfo userDetailInfo = userService.newUser(createUserRequest);

        logger.info(" create user api is done processing");

        return ResponseEntity.ok(userDetailInfo);
    }

    @PostMapping(value = "/login")
    public ResponseEntity<LoginResponse> loginUser(@RequestBody final Authentication authentication) {
        logger.info(" login user api is invoked");
        final LoginResponse loginResponse = userService.login(authentication);
        logger.info(" login user api is done processing");
        return ResponseEntity.ok(loginResponse);
    }

    @PostMapping(value = "/logout")
    public ResponseEntity<String> logout(@RequestParam(value = "username") final String userName) {
        logger.info(" logout api is invoked");
        userService.userLogout(userName);
        logger.info(" logout user api is done processing");
        return ResponseEntity.ok("Success");
    }

    @DeleteMapping(value = "/delete")
    public void deleteUser(@RequestBody final Authentication authentication) {
        userService.deleteUser(authentication);
    }
}
