package com.fams.famspoc.util;

import org.apache.commons.lang3.StringUtils;

import java.util.function.Function;

/**
 * Utility methods for basic argument validation checks (null/empty arguments, etc).
 * <p/>
 * This is very similar to {@code ValidationUtil} in b2s.common.util.  This class is re-done here to allow usage of
 * these utilities without all of the transitive dependencies of b2s.common.util.
 */
public final class Validations {

    /**
     * Throws an {@link IllegalArgumentException} if the {@code objectToTest} is {@code null}, otherwise returns the
     * {@code objectToTest} unchanged.
     * <p/>
     * The {@link #nullCheckMessageFor} method is used to create the message, which can be used in unit tests to verify
     * results.
     *
     * @param <T>          the type of the object being checked for {@code null}
     * @param objectToTest the object to test for {@code null}
     * @param description  the description of the object being tested to include in the {@code null} check message
     * @return the input {@code objectToTest} if it is not {@code null}
     * @see #nullCheckMessageFor
     */
    public static <T> T notNull(final T objectToTest, final String description) {
        return notNull(objectToTest, description, IllegalArgumentException::new);
    }

    /**
     * Throws an exception created by messageToException if the {@code objectToTest} is {@code null}, otherwise returns
     * the {@code objectToTest} unchanged.
     * <p/>
     * Sample usage:
     * <pre>
     * Validations.notNull(stringToTest, description, IllegalStateException::new)
     * </pre>
     * <p/>
     * The {@link #nullCheckMessageFor} method is used to create the message, which can be used in unit tests to verify
     * results.
     *
     * @param <T>                the type of the object being checked for {@code null}
     * @param objectToTest       the object to test for {@code null}
     * @param description        the description of the object being tested to include in the {@code null} check
     *                           message
     * @param messageToException a function that generates a new subclass of RuntimeException from the error message
     * @return the input {@code objectToTest} if it is not {@code null}
     * @see #nullCheckMessageFor
     */
    public static <T> T notNull(
        final T objectToTest,
        final String description,
        final Function<String, ? extends RuntimeException> messageToException) {

        // Do not use local validation methods internally in order to avoid stack overflow
        if (StringUtils.isBlank(description)) {
            throw new IllegalArgumentException(blankCheckMessageFor("description argument to notNull()"));
        }

        if (messageToException == null) {
            throw new IllegalArgumentException(nullCheckMessageFor("messageToException argument to notNull()"));
        }

        if (objectToTest == null) {
            throw messageToException.apply(nullCheckMessageFor(description));
        }

        return objectToTest;
    }

    /**
     * Throws an {@link IllegalArgumentException} if the {@code stringToTest} is blank (null or empty) otherwise returns
     * the {@code objectToTest} unchanged.
     * <p/>
     * The {@link #blankCheckMessageFor} method is used to create the message, which can be used in unit tests to verify
     * results.
     *
     * @param stringToTest the string to test to verify it is not blank
     * @param description  the description of the object being tested to include in the exception message
     * @return the input {@code stringToTest} if it is not blank
     * @see #blankCheckMessageFor
     */
    public static String notBlank(final String stringToTest, final String description) {
        return notBlank(stringToTest, description, IllegalArgumentException::new);
    }

    /**
     * Throws an exception created by the function {@code messageToException} if the {@code stringToTest} is blank (null
     * or empty) otherwise returns the {@code stringToTest} unchanged.
     * <p/>
     * Sample usage:
     * <pre>
     * Validations.notBlank(stringToTest, description, IllegalStateException::new)
     * </pre>
     * <p/>
     * The {@link #blankCheckMessageFor} method is used to create the message, which can be used in unit tests to verify
     * results.
     *
     * @param stringToTest       the string to test to verify it is not blank
     * @param description        the description of the object being tested to include in the exception message
     * @param messageToException a function that generates a new subclass of RuntimeException from the error message
     * @return the input {@code stringToTest} if it is not blank
     * @see #blankCheckMessageFor
     */
    public static String notBlank(
        final String stringToTest,
        final String description,
        final Function<String, ? extends RuntimeException> messageToException) {

        // Do not use local validation methods internally in order to avoid stack overflow
        if (StringUtils.isBlank(description)) {
            throw new IllegalArgumentException(blankCheckMessageFor("description argument to notBlank()"));
        }

        if (messageToException == null) {
            throw new IllegalArgumentException(nullCheckMessageFor("messageToException argument to notBlank()"));
        }

        if (StringUtils.isBlank(stringToTest)) {
            throw messageToException.apply(blankCheckMessageFor(description));
        }

        return stringToTest;
    }

    /**
     * Creates the {@code null} check message used by the {@code null} check methods included in this class.
     *
     * @param description the description of the object being tested to include in the {@code null} check message
     * @return a message for when a {@code null} check of the described object fails
     */
    private static String nullCheckMessageFor(final String description) {
        return "The " + description + " must not be null.";
    }

    /**
     * Creates the blank check message used by the blank check methods included in this class.
     *
     * @param description the description of the object being tested to include in the blank check message
     * @return a message for when a blank check of the described object fails
     */
    private static String blankCheckMessageFor(final String description) {
        return "The " + description + " must not be null or blank.";
    }

    private Validations() {
        super();
    }

}
