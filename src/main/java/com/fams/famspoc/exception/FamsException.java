package com.fams.famspoc.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class FamsException extends RuntimeException {

    private static final long serialVersionUID = 3552642218298786634L;

    public FamsException(final String message) {
        super(message);
    }

    public FamsException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
