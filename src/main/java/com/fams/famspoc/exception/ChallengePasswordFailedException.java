package com.fams.famspoc.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ChallengePasswordFailedException extends RuntimeException {

    private static final long serialVersionUID = 1830364409670992142L;

    public ChallengePasswordFailedException(final String message) {
        super(message);
    }

    public ChallengePasswordFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
