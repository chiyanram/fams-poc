package com.fams.famspoc.services;

import com.fams.famspoc.domain.Authentication;
import com.fams.famspoc.domain.CreateUserRequest;
import com.fams.famspoc.domain.LoginResponse;
import com.fams.famspoc.domain.UserDetailInfo;

public interface UserService {

    UserDetailInfo newUser(CreateUserRequest createUserRequest);

    LoginResponse login(Authentication authentication);

    void userLogout(String userName);

    void deleteUser(Authentication authentication);
}
