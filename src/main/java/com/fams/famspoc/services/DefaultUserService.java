package com.fams.famspoc.services;

import com.amazonaws.services.cognitoidp.model.AWSCognitoIdentityProviderException;
import com.amazonaws.services.cognitoidp.model.NotAuthorizedException;
import com.fams.famspoc.domain.*;
import com.fams.famspoc.exception.BadRequestException;
import com.fams.famspoc.exception.FamsException;
import com.fams.famspoc.exception.UserAlreadyExistsException;
import com.fams.famspoc.repository.UserRepository;
import com.fams.famspoc.util.Validations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DefaultUserService implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(DefaultUserService.class);

    private final UserRepository userRepository;

    public DefaultUserService(final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetailInfo newUser(final CreateUserRequest createUserRequest) {
        try {
            final String userName = createUserRequest.getUserName();
            Validations.notBlank(userName, "username is mandatory.", BadRequestException::new);
            checkUserExists(userName);
            final UserDetailInfo userDetailInfo = userRepository.create(createUserRequest);
            return updateAdditionalInfo(createUserRequest, userDetailInfo);
        } catch (final AWSCognitoIdentityProviderException e) {
            logger.error("Unable to create new user with error message {}", e.getLocalizedMessage(), e);
            throw new FamsException(String.format("Unable to create new user: %s", createUserRequest.getUserName()));
        }
    }

    private static UserDetailInfo updateAdditionalInfo(
        final CreateUserRequest createUserRequest,
        final UserDetailInfo userDetailInfo) {

        final UserDetailInfo.UserDetailInfoBuilder userDetailInfoBuilder = userDetailInfo.toBuilder();
        userDetailInfoBuilder.companyName(createUserRequest.getCompanyName());
        userDetailInfoBuilder.companyUrl(createUserRequest.getCompanyUrl());
        userDetailInfoBuilder.firstName(createUserRequest.getFirstName());
        userDetailInfoBuilder.lastName(createUserRequest.getLastName());
        userDetailInfoBuilder.location(createUserRequest.getLocation());

        return userDetailInfoBuilder.build();
    }

    private void checkUserExists(final String userName) {
        final List<UserDetails> usersByEmail = userRepository.findUsersByEmail(userName);
        if (!usersByEmail.isEmpty()) {
            final String message = String.format("UserName %s exists", userName);
            logger.error(message);
            throw new UserAlreadyExistsException(message);
        }
    }

    @Override
    public LoginResponse login(final Authentication authentication) {
        try {
            return userRepository.login(authentication.getUsername(), authentication.getPassword());
        } catch (final AWSCognitoIdentityProviderException e) {
            logger.error("Unable to login with error message {}", e.getLocalizedMessage(), e);
            throw new FamsException(String.format("Login failed for the user: %s", authentication.getUsername()));
        }
    }

    @Override
    public void userLogout(final String userName) {
        try {
            userRepository.logout(userName);
        } catch (final AWSCognitoIdentityProviderException e) {
            logger.error("Unable to logout with error message {}", e.getLocalizedMessage(), e);
            throw new FamsException(String.format("Unable to logout for the username %s", userName));
        }
    }

    @Override
    public void deleteUser(final Authentication authentication) {
        try {
            userRepository.deleteUser(authentication.getUsername(), authentication.getPassword());
        } catch (final NotAuthorizedException e) {
            logger.error("Unable to delete user with error message {}", e.getLocalizedMessage(), e);
            throw new BadRequestException(String.format(
                "Invalid logout details %s",
                authentication.getUsername()));
        } catch (final AWSCognitoIdentityProviderException e) {
            logger.error("Unable to delete user with error message {}", e.getLocalizedMessage(), e);
            throw new FamsException(String.format(
                "Unable to logout for the username %s",
                authentication.getUsername()));
        }
    }
}
