package com.fams.famspoc.repository;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;
import com.fams.famspoc.domain.UserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.UUID;

@Service
public class AuthenticateAwsDynamoDbImpl implements AuthenticateAwsDynamo {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static final AWSCredentials awsCredentials = new BasicAWSCredentials(awsAccessId, awsSecretKey);
    private static final DynamoDB amazonDynamoDB = new DynamoDB(new AmazonDynamoDBClient(awsCredentials));
    private static final Long readCapacityUnits = 10L;
    private static final Long writeCapacityUnits = 4L;

    private static String generateGuid() {
        return UUID.randomUUID().toString();
    }

    @Override
    public void createTable(final String tableName) {

        final ArrayList<KeySchemaElement> keySchema = new ArrayList<KeySchemaElement>();
        keySchema.add(new KeySchemaElement()
            .withAttributeName("id")
            .withKeyType(KeyType.HASH));

        final ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();
        attributeDefinitions.add(new AttributeDefinition()
            .withAttributeName("id")
            .withAttributeType("N"));
        /*
         * attributeDefinitions.add(new AttributeDefinition()
         * .withAttributeName("userName") .withAttributeType("S"));
         *
         * attributeDefinitions.add(new AttributeDefinition()
         * .withAttributeName("emailAddr") .withAttributeType("S"));
         *
         * attributeDefinitions.add(new AttributeDefinition()
         * .withAttributeName("location") .withAttributeType("S"));
         */

        final CreateTableRequest request = new CreateTableRequest()
            .withTableName(tableName)
            .withKeySchema(keySchema)
            .withProvisionedThroughput(new ProvisionedThroughput()
                .withReadCapacityUnits(readCapacityUnits)
                .withWriteCapacityUnits(writeCapacityUnits))
            .withAttributeDefinitions(attributeDefinitions);
        try {
            final Table table = amazonDynamoDB.createTable(request);
            table.waitForActive();
        } catch (final InterruptedException e) {
            log.info("Create Table results in error");

        }
    }

    @Override
    public void insertDataInTable(final String tableName, final UserDetails userDetails) {

        final Table table = amazonDynamoDB.getTable(tableName);

        try {
            final Item item = new Item()
                .withPrimaryKey("id", generateGuid())
                .withString("userName", userDetails.getUserName())
                .withString("emailAddr", userDetails.getUserName())
                .withString("location", userDetails.getLocation());

            table.putItem(item);
        } catch (final Exception e) {
            log.info("Create Item results in error " + e.getLocalizedMessage());
        }
    }

}
