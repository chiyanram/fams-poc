package com.fams.famspoc.repository;

import com.fams.famspoc.domain.UserDetails;

public interface AuthenticateAwsDynamo {

    String awsAccessId = "aws-access-id";
    String awsSecretKey = "aws-secret-key";

    void createTable(String tableName);

    void insertDataInTable(String tableName, UserDetails userDetails);

}

