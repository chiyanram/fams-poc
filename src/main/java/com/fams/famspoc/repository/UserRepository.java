package com.fams.famspoc.repository;

import com.fams.famspoc.domain.CreateUserRequest;
import com.fams.famspoc.domain.LoginResponse;
import com.fams.famspoc.domain.UserDetailInfo;
import com.fams.famspoc.domain.UserDetails;

import java.util.List;

public interface UserRepository {

    UserDetailInfo create(CreateUserRequest userInfo);

    UserDetails getUserInfo(String userName);

    List<UserDetails> findUsersByEmail(final String email);

    void deleteUser(String userName, String password);

    LoginResponse login(String userName, String password);

    void logout(String userName);
}
