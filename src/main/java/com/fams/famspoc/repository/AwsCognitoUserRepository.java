package com.fams.famspoc.repository;

import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.model.*;
import com.fams.famspoc.domain.*;
import com.fams.famspoc.exception.ChallengePasswordFailedException;
import com.fams.famspoc.exception.DuplicateEmailException;
import com.fams.famspoc.util.Validations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.fams.famspoc.FamsConstants.*;

@Service
public class AwsCognitoUserRepository implements UserRepository {

    private static final Logger logger = LoggerFactory.getLogger(AwsCognitoUserRepository.class);

    private final String poolID;
    private final String clientID;
    private final String dynamoDbGroup;
    private final AWSCognitoIdentityProvider mIdentityProvider;

    public AwsCognitoUserRepository(
        @Value("${aws.user.poolid}") final String poolID,
        @Value("${aws.user.clientid}") final String clientID,
        @Value("${aws.dynamo.group}") final String dynamoDbGroup,
        final AWSCognitoIdentityProvider mIdentityProvider) {

        this.poolID = Validations.notBlank(poolID, "poolID");
        this.clientID = Validations.notBlank(clientID, "clientID");
        this.dynamoDbGroup = Validations.notBlank(dynamoDbGroup, "dynamoDbGroup");
        this.mIdentityProvider = Validations.notNull(mIdentityProvider, "mIdentityProvider");
    }

    @Override
    public UserDetailInfo create(final CreateUserRequest createUserRequest) {

        final String userName = createUserRequest.getUserName();
        final String password =
            StringUtils.hasLength(createUserRequest.getPassword()) ? createUserRequest.getPassword() : TEMP_PASSWORD;

        final AdminCreateUserRequest cognitoRequest =
            new AdminCreateUserRequest()
                .withUserPoolId(poolID)
                .withUsername(userName)
                .withUserAttributes(
                    new AttributeType().withName(EMAIL).withValue(userName),
                    new AttributeType().withName(LOCATION).withValue(createUserRequest.getLocation()),
                    new AttributeType().withName("email_verified").withValue("true"),
                    new AttributeType().withName(NAME).withValue(createUserRequest.getFirstName()))
                .withTemporaryPassword(password).withMessageAction("SUPPRESS");

        final AdminCreateUserResult adminCreateUserResult = mIdentityProvider.adminCreateUser(cognitoRequest);

        challengeFirstLogin(userName, password);

        final UserDetailInfo userDetailInfo = convertCognitoUserType(adminCreateUserResult.getUser(), userName);

        final String accessToken = sessionLogin(userName, password).getAccessToken();

        if (createUserRequest.getIsAdmin() != null && createUserRequest.getIsAdmin()) {
            addUserToGroup(userName, dynamoDbGroup);
        }

        return userDetailInfo.toBuilder().sessionToken(accessToken).build();
    }

    private static UserDetailInfo convertCognitoUserType(final UserType userType, final String userName) {
        return UserDetailInfo.builder()
            .userName(userName).status(userType.getUserStatus()).createdDate(userType.getUserCreateDate())
            .updatedDate(userType.getUserLastModifiedDate())
            .userAttributes(buildUserAttribute(userType.getAttributes()))
            .build();
    }

    private static Map<String, String> buildUserAttribute(final List<AttributeType> attributes) {
        final Map<String, String> userAttribute = new HashMap<>();
        for (final AttributeType attr : attributes) {
            userAttribute.put(attr.getName(), attr.getValue());
        }
        return userAttribute;
    }

    private void challengeFirstLogin(final String userName, final String password) {

        final Map<String, String> initialParams = new HashMap<>();
        initialParams.put(USERNAME, userName);
        initialParams.put(PASS_WORD, password);

        final AdminInitiateAuthRequest initialRequest = new AdminInitiateAuthRequest()
            .withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
            .withAuthParameters(initialParams)
            .withClientId(clientID)
            .withUserPoolId(poolID);

        final AdminInitiateAuthResult initialResponse = mIdentityProvider.adminInitiateAuth(initialRequest);

        if (!ChallengeNameType.NEW_PASSWORD_REQUIRED.name().equals(initialResponse.getChallengeName())) {
            throw new ChallengePasswordFailedException(
                String.format(" Challenge Password Failed for the username %s", userName));
        }

        if (StringUtils.hasLength(password)) {
            final Map<String, String> challengeResponses = new HashMap<>();
            challengeResponses.put(USERNAME, userName);
            challengeResponses.put(NEW_PASSWORD, password);

            final AdminRespondToAuthChallengeRequest finalRequest =
                new AdminRespondToAuthChallengeRequest()
                    .withChallengeName(ChallengeNameType.NEW_PASSWORD_REQUIRED)
                    .withChallengeResponses(challengeResponses)
                    .withClientId(clientID)
                    .withUserPoolId(poolID)
                    .withSession(initialResponse.getSession());

            // Invokes the cognito authentication
            final AdminRespondToAuthChallengeResult challengeResponse =
                mIdentityProvider.adminRespondToAuthChallenge(finalRequest);

            if (!(challengeResponse.getChallengeName() == null
                      || challengeResponse.getChallengeName().isEmpty())) {
                throw new ChallengePasswordFailedException(
                    "The User has another challege address " + userName + " is already in the database");
            }
        }
    }

    private void addUserToGroup(final String username, final String groupName) {
        try {
            final AdminAddUserToGroupRequest addUserToGroupRequest =
                new AdminAddUserToGroupRequest()
                    .withGroupName(groupName)
                    .withUserPoolId(poolID)
                    .withUsername(username);
            mIdentityProvider.adminAddUserToGroup(addUserToGroupRequest);
        } catch (final RuntimeException e) {
            throw new DuplicateEmailException(e.getMessage());
        }
    }

    @Override
    public void deleteUser(final String userName, final String password) {
        final UserSession userSession = sessionLogin(userName, password);
        logger.debug("Session is active {}", userSession.getAccessToken());
        final AdminDeleteUserRequest deleteRequest =
            new AdminDeleteUserRequest()
                .withUsername(userName)
                .withUserPoolId(poolID);
        mIdentityProvider.adminDeleteUser(deleteRequest);
    }

    @Override
    public List<UserDetails> findUsersByEmail(final String email) {

        final String emailQuery = "email=\"" + email + "\"";

        final ListUsersRequest usersRequest =
            new ListUsersRequest()
                .withUserPoolId(poolID)
                .withAttributesToGet(EMAIL, LOCATION)
                .withFilter(emailQuery);

        final ListUsersResult searchResults = mIdentityProvider.listUsers(usersRequest);

        return searchResults.getUsers()
            .stream()
            .map(toUserInfo())
            .collect(Collectors.toList());

    }

    private static Function<UserType, UserDetails> toUserInfo() {
        return user -> {
            final UserDetails.UserDetailsBuilder userDetails = UserDetails.builder();
            final List<AttributeType> attributes = user.getAttributes();
            for (final AttributeType attr : attributes) {
                if (attr.getName().equals(EMAIL)) {
                    userDetails.userName(attr.getValue());
                } else if (attr.getName().equals(LOCATION)) {
                    userDetails.location(attr.getValue());
                }
            }
            return userDetails.build();
        };
    }

    /**
     * @param userName
     * @param password
     * @return
     * @throws AWSCognitoIdentityProviderException
     */
    @Override
    public LoginResponse login(final String userName, final String password) {
        final UserSession userSession = sessionLogin(userName, password);

        final UserDetails userDetails = getUserInfo(userName);

        final LoginResponse.LoginResponseBuilder loginResponse = LoginResponse.builder();
        loginResponse.userName(userDetails.getUserName());
        loginResponse.location(userDetails.getLocation());

        final String challengeResult = userSession.getChallengeResult();

        if (challengeResult != null && !challengeResult.isEmpty()) {
            loginResponse.newPasswordRequired(challengeResult.equals(ChallengeNameType.NEW_PASSWORD_REQUIRED.name()));
        }

        loginResponse.sessionToken(userSession.getAccessToken());

        return loginResponse.build();
    }

    @Override
    public void logout(final String userName) {
        final AdminUserGlobalSignOutRequest signOutRequest =
            new AdminUserGlobalSignOutRequest()
                .withUsername(userName)
                .withUserPoolId(poolID);
        mIdentityProvider.adminUserGlobalSignOut(signOutRequest);
    }

    /**
     * @param userName
     * @param password
     * @return
     * @throws AWSCognitoIdentityProviderException
     */
    private UserSession sessionLogin(final String userName, final String password) {
        final Map<String, String> authParams = new HashMap<>();
        authParams.put(USERNAME, userName);
        authParams.put(PASSWORD, password);

        final AdminInitiateAuthRequest authRequest = new AdminInitiateAuthRequest()
            .withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
            .withUserPoolId(poolID)
            .withClientId(clientID)
            .withAuthParameters(authParams);

        final AdminInitiateAuthResult authResult = mIdentityProvider.adminInitiateAuth(authRequest);

        final AuthenticationResultType resultType = authResult.getAuthenticationResult();

        return UserSession.builder()
            .session(authResult.getSession())
            .accessToken(resultType.getAccessToken())
            .challengeResult(authResult.getChallengeName())
            .build();
    }

    /**
     * @param userName
     * @return
     * @throws AWSCognitoIdentityProviderException
     */
    @Override
    public UserDetails getUserInfo(final String userName) {
        final AdminGetUserRequest userRequest = new AdminGetUserRequest().withUsername(userName).withUserPoolId(poolID);
        final AdminGetUserResult userResult = mIdentityProvider.adminGetUser(userRequest);
        final List<AttributeType> userAttributes = userResult.getUserAttributes();
        final UserDetails.UserDetailsBuilder userDetailsBuilder = UserDetails.builder();
        for (final AttributeType attr : userAttributes) {
            if (attr.getName().equals(EMAIL)) {
                userDetailsBuilder.userName(attr.getValue());
            } else if (attr.getName().equals(LOCATION)) {
                userDetailsBuilder.location(attr.getValue());
            }
        }
        return userDetailsBuilder.build();
    }
}
