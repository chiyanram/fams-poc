package com.fams.famspoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FamsPocApplication {

    public static void main(final String[] args) {
        SpringApplication.run(FamsPocApplication.class, args);
    }

}
