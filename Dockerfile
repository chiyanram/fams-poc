FROM java:8-jdk-alpine
COPY ./target/fams-poc-0.0.1-SNAPSHOT.jar fams-poc.jar
ENTRYPOINT ["java", "-jar", "fams-poc.jar", "--spring.profiles.active=dev"]